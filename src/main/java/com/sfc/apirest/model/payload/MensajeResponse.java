package com.sfc.apirest.model.payload;

import lombok.*;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
@Builder
public class MensajeResponse implements Serializable {
    private String mnesaje;
    private Object object;
}
