package com.sfc.apirest.controller;

import com.sfc.apirest.model.dto.ClienteDto;
import com.sfc.apirest.model.entity.Cliente;
import com.sfc.apirest.model.payload.MensajeResponse;
import com.sfc.apirest.service.IClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class ClienteController {
    @Autowired
    private IClienteService clienteService;

    @GetMapping("clientes")
//    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> showAll() {
        List<Cliente> getList = clienteService.listAll();
        if (getList == null) {
            return new ResponseEntity<>(
                    MensajeResponse
                            .builder()
                            .mnesaje("No hay registros")
                            .object(null)
                            .build(),
                    HttpStatus.OK);
        }
        return new ResponseEntity<>(
                MensajeResponse
                        .builder()
                        .mnesaje("")
                        .object(getList)
                        .build(),
                HttpStatus.OK);
    }


    @PostMapping("cliente")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> create(@RequestBody ClienteDto clienteDto) {
        Cliente clienteSave = null;
        try {
            clienteSave = clienteService.save(clienteDto);
            return new ResponseEntity<>(
                    MensajeResponse
                            .builder()
                            .mnesaje("Guardado correctamente")
                            .object(
                                    ClienteDto
                                            .builder()
                                            .idCliente(clienteSave.getIdCliente())
                                            .nombre(clienteSave.getNombre())
                                            .apellido(clienteSave.getApellido())
                                            .correo(clienteSave.getCorreo())
                                            .fechaRegistro(clienteSave.getFechaRegistro())
                                            .build())
                            .build(),
                    HttpStatus.CREATED);
        } catch (DataAccessException exDt) {
            return new ResponseEntity<>(
                    MensajeResponse
                            .builder()
                            .mnesaje(exDt.getMessage())
                            .object(null),
                    HttpStatus.METHOD_NOT_ALLOWED);
        }
    }

    @PutMapping("cliente/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> update(@RequestBody ClienteDto clienteDto,
                                    @PathVariable Integer id) {
        Cliente clienteUpdate = null;
        try {
//            Cliente findCliente = clienteService.findById(id);
            if(clienteService.existsById(id)){
                clienteDto.setIdCliente(id);
                clienteUpdate = clienteService.save(clienteDto);
                return new ResponseEntity<>(
                        MensajeResponse
                                .builder()
                                .mnesaje("Guardado correctamente")
                                .object(
                                        ClienteDto
                                                .builder()
                                                .idCliente(clienteUpdate.getIdCliente())
                                                .nombre(clienteUpdate.getNombre())
                                                .apellido(clienteUpdate.getApellido())
                                                .correo(clienteUpdate.getCorreo())
                                                .fechaRegistro(clienteUpdate.getFechaRegistro())
                                                .build())
                                .build(),
                        HttpStatus.CREATED);
            }else {
                return new ResponseEntity<>(
                        MensajeResponse
                                .builder()
                                .mnesaje("El registro que intenta actualizar no se encuentra en la base de datos.")
                                .object(null)
                                .build(),
                        HttpStatus.NOT_FOUND);
            }
        } catch (DataAccessException exDt) {
            return new ResponseEntity<>(
                    MensajeResponse
                            .builder()
                            .mnesaje(exDt.getMessage())
                            .object(null)
                            .build(),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("cliente/{id}")
//    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        try {
            Cliente clienteDelete = clienteService.findById(id);
            clienteService.delete(clienteDelete);
            return new ResponseEntity<>(clienteDelete, HttpStatus.NO_CONTENT);
        } catch (DataAccessException exDt) {
            return new ResponseEntity<>(
                    MensajeResponse
                            .builder()
                            .mnesaje(exDt.getMessage())
                            .object(null)
                            .build(),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("cliente/{id}")
//    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> showById(@PathVariable Integer id) {
        Cliente cliente = clienteService.findById(id);
        if (cliente == null) {
            return new ResponseEntity<>(
                    MensajeResponse
                            .builder()
                            .mnesaje("El registro que intenta buscar, no existe!!")
                            .object(null)
                            .build(),
                    HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(
                MensajeResponse
                        .builder()
                        .mnesaje("")
                        .object(ClienteDto
                                .builder()
                                .idCliente(cliente.getIdCliente())
                                .nombre(cliente.getNombre())
                                .apellido(cliente.getApellido())
                                .correo(cliente.getCorreo())
                                .fechaRegistro(cliente.getFechaRegistro())
                                .build())
                        .build(),
                HttpStatus.OK);
    }
}
